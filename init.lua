if minetest.get_modpath("technic") then
    minetest.register_alias("zinc:stone_with_zinc", "technic:mineral_zinc")
    minetest.register_alias("zinc:zinc_lump", "technic:zinc_lump")
    minetest.register_alias("zinc:zinc_ingot", "technic:zinc_ingot")
    minetest.register_alias("zinc:zinc_block", "technic:zinc_block")
else
    minetest.register_craftitem("zinc:zinc_lump", {
        description = "Zinc Lump",
        inventory_image = "zinc_lump.png",
    })
    
    minetest.register_craftitem("zinc:zinc_ingot", {
        description = "Zinc Ingot",
        inventory_image = "zinc_ingot.png",
    })
    
    minetest.register_craft({
        type = "cooking",
        recipe = "zinc:zinc_lump",
        output = "zinc:zinc_ingot",
    })
    
    minetest.register_node("zinc:stone_with_zinc", {
        description = "Zinc Ore",
        tiles = {"default_stone.png^zinc_mineral_zinc.png"},
        groups = {cracky=3},
        sounds = default.node_sound_stone_defaults(),
        drop = "zinc:zinc_lump",
    })
    
    minetest.register_ore({
        ore_type = "scatter",
        ore = "zinc:stone_with_zinc",
        wherein = "default:stone",
        clust_scarcity = 13*13*13,
        clust_num_ores = 3,
        clust_size = 2,
        y_min = -31000,
        y_max = -128,
    })
    
    minetest.register_node("zinc:zinc_block", {
        description = "Zinc Block",
        tiles = {"zinc_block.png"},
        groups = {cracky=1, level=2},
        sounds = default.node_sound_metal_defaults()
    })
    
    minetest.register_craft({
        output = "zinc:zinc_block",
        recipe = {
            {"zinc:zinc_ingot", "zinc:zinc_ingot", "zinc:zinc_ingot"},
            {"zinc:zinc_ingot", "zinc:zinc_ingot", "zinc:zinc_ingot"},
            {"zinc:zinc_ingot", "zinc:zinc_ingot", "zinc:zinc_ingot"},
        },
    })
    
    minetest.register_craft({
        output = "zinc:zinc_ingot 9",
        recipe = {{"zinc:zinc_block"}},
    })
    
    minetest.clear_craft({
        output = "basic_materials:brass_ingot",
    })
    
    minetest.register_craft({
        output = "basic_materials:brass_ingot 9",
        recipe = {{"basic_materials:brass_block"}},
    })
    
    minetest.register_craft( {
        output = "basic_materials:brass_ingot 3",
        recipe = {
            {"default:copper_ingot"},
            {"default:copper_ingot"},
            {"zinc:zinc_ingot"},
        },
    })
    
    if minetest.get_modpath("terumet") then
        terumet.register_ore_cutting("zinc:stone_with_zinc")
        
        local crys_zinc = terumet.register_crystal({
            suffix = "zinc",
            color = "#d4ebf1",
            name = "Crystallized Zinc",
            cooking_result = "zinc:zinc_ingot"
        })
        
        terumet.register_vulcan_result("zinc:zinc_lump", crys_zinc)
        terumet.register_vulcan_result("zinc:stone_with_zinc", crys_zinc, 1)
    end
    
    if minetest.get_modpath("lucky_block") then
        lucky_block:add_blocks({
            {"nod", "zinc:zinc_block", 0},
            {"nod", "zinc:stone_with_zinc", 0},
            {"dro", "zinc:zinc_ingot", 4},
        })
    end
    
    if minetest.get_modpath("dungeon_loot") then
        dungeon_loot.register({
            name = "zinc:zinc_ingot",
            chance = 0.3,
            count = {1, 5},
            y = {-31000, -64},
        })
    end
end
